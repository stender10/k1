public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      // for debugging
      Color[] balls = new Color[]{Color.blue, Color.blue, Color.green, Color.red, Color.green};
      reorder(balls);

      for (Color ball : balls) {
         System.out.println(ball);
      }
   }
   
   public static void reorder (Color[] balls) {
      // TODO!!! Your program here

      int red = 0;
      int blue = balls.length - 1;

      for (int i = 0; i <= blue; i++) {
         if (red < blue) {
            if (balls[i] == Color.red) {
               Color tmp = balls[red];
               balls[red] = balls[i];
               balls[i] = tmp;
               red++;
            } else if (balls[i] == Color.blue) {
               Color tmp = balls[blue];
               balls[blue] = balls[i];
               balls[i] = tmp;
               i--; blue--;
            }
         }
      }
   }
}

